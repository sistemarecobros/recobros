<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Manager Routes
|--------------------------------------------------------------------------
|
| Here is where you can register manager routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "manager" middleware group. Now create something great!
|
*/


Route::resource('user', 'UserController');
Route::get('user/destroy/{id}', ['as'=> 'user/destroy', 'uses' => 'UserController@destroy']);
Route::post('user/show', ['as' => 'user/show', 'uses' => 'UserController@show']);
Route::post('user/update/{id}', ['as' => 'user/update', 'uses' => 'UserController@update']);

Route::resource('az', 'AZController');
Route::get('az/destroy/{id}', ['as'=> 'az/destroy', 'uses' => 'AZController@destroy']);
Route::post('az/show', ['as' => 'az/show', 'uses' => 'AZController@show']);
Route::post('az/update/{id}', ['as' => 'az/update', 'uses' => 'AZController@update']);

Route::resource('perfil', 'PerfilController');
Route::post('perfil/show', ['as' => 'perfil/show', 'uses' => 'PerfilController@show']);
Route::post('perfil/update/{id}', ['as' => 'perfil/update', 'uses' => 'PerfilController@update']);