<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Employee Routes
|--------------------------------------------------------------------------
|
| Here is where you can register employee routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "employee" middleware group. Now create something great!
|
*/

Route::resource('az', 'AZController');
Route::get('az/destroy/{id}', ['as'=> 'az/destroy', 'uses' => 'AZController@destroy']);
Route::post('az/show', ['as' => 'az/show', 'uses' => 'AZController@show']);
Route::post('az/update/{id}', ['as' => 'az/update', 'uses' => 'AZController@update']);

Route::resource('perfil', 'PerfilController');
Route::post('perfil/show', ['as' => 'perfil/show', 'uses' => 'PerfilController@show']);
Route::post('perfil/update/{id}', ['as' => 'perfil/update', 'uses' => 'PerfilController@update']);