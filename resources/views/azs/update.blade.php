@extends('layouts.master')
@section('content')
<script>

  (function() {
    'use strict';
    window.addEventListener('load', function() {
            
    var forms = document.getElementsByClassName('needs-validation');
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
</script>
                          
<div class="card">
    <div class="card-header card-header-info">
        <h4 class="card-title ">AZ's</h4>
        <p class="card-category">Editar información de la AZ</p>
    </div>
    <div class="card-body ">
        <form action="{{url(\Auth::user()->urlAZUpdate($az->id))}}" method="post" class="needs-validation" novalidate>
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col">
                        <label>Numero de AZ</label>
                        <input  type="text" name="number_AZ" class="form-control" value="{{$az->number_AZ}}" id="validationCustom01" required >
                        <div class="valid-feedback">
                            Correcto!
                        </div>
                        <div class="invalid-feedback">
                            Complete el recuadro
                        </div>
                    </div>
                    <div class="col">
                        <label >Nombre demanda</label>
                        <input  type="text" name="nombre_demanda"class="form-control" value="{{$az->nombre_demanda}}"  id="validationCustom01" required >
                        <div class="valid-feedback">
                            Correcto!
                        </div>
                        <div class="invalid-feedback">
                            Complete el recuadro
                        </div>
                    </div>
                    <div class="col">
                        <label >Apoderado</label>
                        <input  type="text" name="apoderado" class="form-control" value="{{$az->apoderado}}" id="validationCustom01" required>
                        <div class="valid-feedback">
                            Correcto!
                        </div>
                        <div class="invalid-feedback">
                            Complete el recuadro
                        </div>
                    </div>
                                    
                    <div class="row">
                        <div class="col">
                            <label>Cantidad items </label>
                            <input  type="number" name="cantidad_items" class="form-control" value="{{$az->cantidad_items}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Cantidad recobros</label>
                            <input  type="number" name="cantidad_recobros"class="form-control" value="{{$az->cantidad_recobros}}"  id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Vlr base capital</label>
                            <input  type="number" name="vlr_base_capital"class="form-control" value="{{$az->vlr_base_capital}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Gastos administrativos 10%</label>
                            <input  type="number" name="gastos_administrativos_10" class="form-control" value="{{$az->gastos_administrativos_10}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label>Cuantia:</label>
                            <input  type="number" name="cuantia" class="form-control" value="{{$az->cuantia}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >RA</label>
                            <input  type="text" name="RA" class="form-control" value="{{$az->RA}}"  id="validationCustom01" required>           
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label >RTA RA</label>
                            <input  type="text" name="RTA_RA" class="form-control" class="col" value="{{$az->RTA_RA}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Conciliación:</label>
                            <input type="text" name="conciliacion" class="form-control" class="col" value="{{$az->conciliacion}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Demanda administrativa:</label>
                            <input type="text" name="demanda_administrativa" class="form-control" class="col" value="{{$az->demanda_administrativa}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Demanda laboral:</label>
                            <input  type="text" name="demanda_laboral" class="form-control" value="{{$az->demanda_laboral}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>                    
                        <div class="col">
                            <label >Pagos:</label>
                            <input  type="text" name="pagos"class="form-control"  value="{{$az->pagos}}" id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >POS:</label>
                            <input  type="text" name="POS"class="form-control"   value="{{$az->POS}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Desistidos:</label>
                            <input  type="text" name="desistidos" class="form-control" value="{{$az->desistidos}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >AGS:</label>
                            <input  type="text" name="AGS"class="form-control"  value="{{$az->AGS}}" id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Investigación:</label>
                            <input  type="text" name="investigacion"class="form-control" value="{{$az->investigacion}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Adecuación</label>
                            <input  type="text" name="adecuacion" class="form-control" value="{{$az->adecuacion}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Subsanación</label>
                            <input  type="text" name="subsanacion"class="form-control" value="{{$az->subsanacion}}"  id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Priorida APF:</label>
                            <input  type="text" name="prioridad_APF" class="form-control" value="{{$az->prioridad_APF}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Constancia:</label>
                            <input  type="text" name="constancia" class="form-control" value="{{$az->constancia}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Demanda:</label>
                            <input  type="text" name="demanda" class="form-control" value="{{$az->demanda}}" id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Auto admisorio demanda:</label>
                            <input  type="text" name="auto_admisorio_demanda" class="form-control" value="{{$az->auto_admisorio_demanda}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>                   
                    <div class="row">
                        <div class="col">
                            <label>Notificación autoadmisorio demanda al demandante:</label>
                            <input  type="text" name="notificacion_autoadmisorio_demanda_al_demandante" class="form-control" value="{{$az->notificacion_autoadmisorio_demanda_al_demandante}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Notificación demanda al demandado:</label>
                            <input  type="text" name="notificacion_demanda_al_demandado"class="form-control" value="{{$az->notificacion_demanda_al_demandado}}" id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >No. inicial del proceso judicial:</label>
                            <input  type="number" name="numero_inicial_del_proceso_judicial" class="form-control" value="{{$az->numero_inicial_del_proceso_judicial}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Nombre inicial del despacho judicial:</label>
                            <input  type="text" name="nombre_inicial_del_despacho_judicial" class="form-control" value="{{$az->nombre_inicial_del_despacho_judicial}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >No. actual del proceso judicial:</label>
                            <input  type="number" name="numero_actual_del_proceso_judicial" class="form-control" value="{{$az->numero_actual_del_proceso_judicial}}" id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Nombre actual del despacho judicial:</label>
                            <input  type="text" name="nombre_actual_del_despacho_judicial"class="form-control" value="{{$az->nombre_actual_del_despacho_judicial}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Pendiente resolver conflicto de competencia:</label>
                            <input  type="text" name="pendiente_resolver_conflicto_de_competencia" class="form-control" value="{{$az->pendiente_resolver_conflicto_de_competencia}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >Aprobado por glosa pendiente aprobación desistimiento:</label>
                            <input  type="text" name="aprobado_por_glosa_pendiente_aprobacion_desistimiento"class="form-control" value="{{$az->aprobado_por_glosa_pendiente_aprobacion_desistimiento}}" id="validationCustom01" required >
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label >En investigación por parte de la contraloria:</label>
                            <input  type="text" name="en_investigacion_por_parte_de_la_contraloria"class="form-control" value="{{$az->en_investigacion_por_parte_de_la_contraloria}}" id="validationCustom01" required>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col">
                            <label>Numero de la investigacion:</label>
                            <input  type="text" name="numero_de_la_investigacion" class="form-control" id="validationCustom01" required value="{{$az->numero_de_la_investigacion}}">
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label>Fecha prestamo:</label>
                            <input  type="text" name="fecha_prestamo" class="form-control" id="validationCustom01" required value="{{$az->fecha_prestamo}}">
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                        <div class="col">
                            <label>Fecha devolución:</label>
                            <input  type="text" name="fecha_devolucion" class="form-control" id="validationCustom01" required value="{{$az->fecha_devolucion}}">
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label>Nombre abogado:</label>
                        <input  type="text" name="nombre_abogado" class="form-control" id="validationCustom01" required value="{{$az->nombre_abogado}}">
                        <div class="valid-feedback">
                            Correcto!
                        </div>
                        <div class="invalid-feedback">
                            Complete el recuadro
                        </div>
                    </div>
                    <div class="col">
                            <label >Usuario:</label>
                            <select name="user_id"  class="form-control" id="validationCustom01" required>
                                <option value="">Seleccione...</option>
                                    @foreach($users as $user)
                                        @if($user->id == $az->user_id)
                                            <option value="{{$user->id}}" selected>{{$user->name}}</option>
                                        @else 
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                            <div class="valid-feedback">
                                Correcto!
                            </div>
                            <div class="invalid-feedback">
                                Complete el recuadro
                            </div>
                    </div>  
                </div>
                    <div class="form-group">                                    
                         <button type="submit" class="btn btn-info float-right ">Actualizar</button>
                    </div> 
            </div> 
        </form>
    </div>
</div>
@endsection