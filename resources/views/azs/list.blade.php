@extends('layouts.master')

@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
      <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <div>
                <!--<h2 class="mb-0">Gestion Usuarios</h3>-->
              </div><br>
              <form action="{{url(Auth::user()->urlAZShow())}}" method="post" novalidate class="form-inline">
                @csrf
                  <div class="form-group">
                    <label>Numero de az: </label><br><br><br>
                    <input type="Ttext" name="number_az" class="form-control" placeholder="Buscar por numero de AZ">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-info ml-2 mr-2"><i class="material-icons">search</i></button>
                    <a href="{{url(Auth::user()->urlAZAll())}}" class="btn btn-warning"><i class="material-icons">list</i></a><br>
                    @if(Auth::user()->role->name != 'Abogado')
                      <a href="{{url(Auth::user()->urlAZCreate())}}" class="btn btn-success"><i class="bi bi-pencil-square"></i></a>
                    @endif
                  </div>
              </form>
     </div>
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title ">AZ's</h4>
            <p class="card-category"> Aqui esta toda la información de las AZ del sistema</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-info">
                  <th>AZ</th>
                  <th>Nombre de demanda</th>
                  <th>Apoderado</th>
                  <th>Cantidad items</th>
                  <th>Cantidad recobros</th>
                  <th>vlr base capital $</th>
                  <th>Gastos administrativos 10% $</th>
                  <th>Cuantia $</th>
                  <th>RA</th>
                  <th>RTA RA</th>
                  <th>Conciliación</th>
                  <th>Demanda administrativa</th>
                  <th>Demanda laboral</th>
                  <th>Pagos</th>
                  <th>POS</th>
                  <th>Desistidos</th>
                  <th>AGS</th>
                  <th>Investigación</th>
                  <th>Adecuación</th>
                  <th>Subsanación</th>
                  <th>Prioridad APF</th>   
                  <th>Constancia</th>
                  <th>Demanda</th>  
                  <th>Auto admisorio demanda</th> 
                  <th>Notificación autoadmisorio demanda al demandante</th> 
                  <th>Notificación demanda al demandado</th> 
                  <th>No. inicial proceso judicial</th> 
                  <th>Nombre inicial del despacho judicial</th> 
                  <th>No. actual del proceso judicial</th> 
                  <th>Nombre actual del despacho judicial</th> 
                  <th>¿Pendiente resolver conflicto de competencia?</th> 
                  <th>¿Aprobado por glosa pendiente aprobación desistimiento?</th>
                  <th>¿En investigación por parte de la contraloria?</th>
                  <th>No. de la investigacion</th>
                  <th>Fecha prestamo</th>
                  <th>Fecha devolución</th>
                  <th>Nombre abogado</th>
                  <th>Usuario</th>
                  @if(Auth::user()->role->name != 'Abogado')
                    <th>Acciones</th>
                  @endif 
                </thead>
                <tbody>
                @foreach($azs as $az)
                    <tr>
                      <td>{{$az->number_AZ}}</td>
                      <td>{{$az->nombre_demanda}}</td>
                      <td>{{$az->apoderado}}</td>
                      <td>{{$az->cantidad_items}}</td>
                      <td>{{$az->cantidad_recobros}}</td>
                      <td>${{$az->vlr_base_capital}}</td>
                      <td>${{$az->gastos_administrativos_10}}</td>
                      <td>${{$az->cuantia}}</td>
                      <td>{{$az->RA}}</td>
                      <td>{{$az->RTA_RA}}</td>
                      <td>{{$az->conciliacion}}</td>
                      <td>{{$az->demanda_administrativa}}</td>
                      <td>{{$az->demanda_laboral}}</td>
                      <td>{{$az->pagos}}</td>
                      <td>{{$az->POS}}</td>
                      <td>{{$az->desistidos}}</td>
                      <td>{{$az->AGS}}</td>
                      <td>{{$az->investigacion}}</td>
                      <td>{{$az->adecuacion}}</td>
                      <td>{{$az->subsanacion}}</td>
                      <td>{{$az->prioridad_APF}}</td>
                      <td>{{$az->constancia}}</td>
                      <td>{{$az->demanda}}</td>
                      <td>{{$az->auto_admisorio_demanda}}</td>
                      <td>{{$az->notificacion_autoadmisorio_demanda_al_demandante}}</td>
                      <td>{{$az->notificacion_demanda_al_demandado}}</td>
                      <td>{{$az->numero_inicial_del_proceso_judicial}}</td>
                      <td>{{$az->nombre_inicial_del_despacho_judicial}}</td>
                      <td>{{$az->numero_actual_del_proceso_judicial}}</td>
                      <td>{{$az->nombre_actual_del_despacho_judicial}}</td>
                      <td>{{$az->pendiente_resolver_conflicto_de_competencia}}</td>
                      <td>{{$az->aprobado_por_glosa_pendiente_aprobacion_desistimiento}}</td>
                      <td>{{$az->en_investigacion_por_parte_de_la_contraloria}}</td>
                      <td>{{$az->numero_de_la_investigacion}}</td>
                      <td>{{$az->fecha_prestamo}}</td>
                      <td>{{$az->fecha_devolucion}}</td>
                      <td>{{$az->nombre_abogado}}</td>
                      <td>{{$az->user->name}}</td>
                      <td>
                      @if(Auth::user()->role->name != 'Abogado')
                        <a class="btn btn-info btn-sm my-2 "  
                        href="{{url(Auth::user()->urlAZEdit($az->id))}}"><i class="material-icons">edit</i>
                        </a>
                      @endif
                        
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
  
@endsection