<ul class="nav navbar-nav nav-menu">
      </ul><ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{url('manager/user')}}">
          <i class="material-icons">content_paste</i>
            <p>Usuarios</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url('manager/az')}}">
          <i class="material-icons">library_books</i>
            <p>AZ's</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url( Auth::user()->urlUserEditPerfil(Auth::user()->id) ) }}">
          <i class="material-icons">people</i>
          <p>Perfil</p>
        </a>
      </li>
</ul>