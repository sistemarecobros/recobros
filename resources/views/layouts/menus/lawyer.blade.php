<ul class="nav navbar-nav nav-menu">
     
      <li class="nav-item">
        <a class="nav-link" href="{{url('lawyer/az')}}">
          <i class="material-icons">library_books</i>
            <p>AZ's</p>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{url( Auth::user()->urlUserEditPerfil(Auth::user()->id) ) }}">
          <i class="material-icons">people</i>
          <p>Perfil</p>
        </a>
      </li>
</ul>