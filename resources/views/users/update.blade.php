@extends('layouts.master')

@section('content')
<script>

  (function() {
    'use strict';
    window.addEventListener('load', function() {
            
    var forms = document.getElementsByClassName('needs-validation');
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
</script>
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title ">Editar usuario</h4>
            <p class="card-category"> Aqui esta toda la informacion de los usuarios del sistema</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
            <form action="{{url(\Auth::user()->urlUserUpdate($user->id))}}" method="post" class="needs-validation" novalidate>
          @csrf
        <div class="container">
          <div class="row">
            <div class="col">
              <label for="validationCustom01">Nombre</label>
              <input type="text" name="name"  class="form-control" id="validationCustom01" required value='{{$user->name}}'>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
            <div class="col">
              <label >Apellido</label>
              <input  type="text" name="lastname"  class="form-control" id="validationCustom02" required value='{{$user->lastname}}'>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <label>Telefono</label>
              <input  type="number" name="phone" class="form-control" id="validationCustom03"  required pattern="" value='{{$user->phone}}'>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
            <div class="col">
              <label >Documento</label>
              <input  type="number" name="document" class="form-control" id="validationCustom04" required value='{{$user->document}}'>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <label>Dirección</label>
              <input  type="text" name="address"  class="form-control" id="validationCustom05" required value='{{$user->address}}'>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
            <div class="col">
              <div class="row">
                <div class="col">
                  <label >Email</label>
                    <input  type="Email" name="email"  class="form-control" id="validationCustom06" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z0-9_]{1,5}" value='{{$user->email}}'>
                    <div class="valid-feedback">
                      Correcto!
                    </div>
                    <div class="invalid-feedback">
                      Complete el recuadro
                    </div>
                </div>
              </div>
            </div>
            <div class="col">
              <label>Estado</label>
              <select name="status_id"  class="form-control" id="validationCustom07" required>
                <option value="">Seleccione...</option>
                @foreach($statuses as $status)
                    @if($status->id == $user->status_id)
                        <option value="{{$status->id}}" selected>{{$status->name}}</option>
                    @else
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endif
                @endforeach
              </select>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
             </div>
            </div>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-info float-right">Actualizar</button> 
          </div>      
        </form>  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection
