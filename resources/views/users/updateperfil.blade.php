@extends('layouts.master')
@section('content')
  <!-- Main content -->
  
    <!-- Header -->
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 450px; background-image: url({{asset('assets/img/wepik.png')}}); background-size: cover; background-position: center top;">
      <!-- Mask -->
      <span class="mask  opacity-8"></span>
      <!-- Header container -->
      <div class="container-fluid d-flex align-items-center">
        <div class="row">
          <div class="col-lg-7 col-md-10">
            <h1 class="display-2 text-dark">Hola {{ Auth::user()->name }}</h1>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="{{asset('assets/img/usuario.png')}} " class="rounded-circle">
                  </a>
                </div>
              </div>
            </div>
            <div class="card-body pt-0">
              <div class="text-center">
                <h5 class="h3">
                  {{Auth::user()->name}}
                </h5>
                <div class="h5 font-weight-300">
                  <i class="ni location_pin mr-2"></i>{{Auth::user()->role->name}}
                </div>
                <div class="h5 mt-4">
                  <i class="ni business_briefcase-24 mr-2"></i>SI Recobros/Juridica
                </div>
                <div>
                  <i class="ni education_hat mr-2"></i>EPS Sanitas
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Editar perfil </h3>
                </div>
                <div class="col-4 text-right">
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="{{url( \Auth::user()->urlUserUpdatePerfil($user->id) ) }}" method="post">
                @csrf
                <h6 class="heading-small text-muted mb-4">Información del usuario</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Nombre</label><br>
                        <input type="text" name="name" class="form-control" id="input-username" class="col" value="{{$user->name}}">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Apellido</label><br>
                        <input  type="text" id="input-email" class="form-control" name="lastname" class="col" value="{{$user->lastname}}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">Documento</label><br>
                        <input  type="number" id="input-first-name" name="document"class="form-control"  value="{{$user->document}}" >
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Email</label><br>
                        <input  type="text" id="input-last-name" name="email" class="form-control" class="col" value="{{$user->email}}">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Contraseña</label><br>
                        <input  type="password" id="input-last-name" name="password" class="form-control" class="col" value="{{$user->password}}">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Información de contacto</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Dirección</label><br>
                        <input id="input-address" type="text" name="address" class="form-control"  value="{{$user->address}}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city">Telefono</label><br>
                        <input  type="number" id="input-city" name="phone" class="form-control" value="{{$user->phone}}">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <button type="submit" class="btn btn-info float-right ">Actualizar</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
    </div>
  </div>
@endsection