@extends('layouts.master')

@section('content')
<script>

  (function() {
    'use strict';
    window.addEventListener('load', function() {
            
    var forms = document.getElementsByClassName('needs-validation');
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
</script>

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
      <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <div>
                <!--<h2 class="mb-0">Gestion Usuarios</h3>-->
              </div><br>
              <form action="{{url(Auth::user()->urlUserShow())}}" method="post" novalidate class="form-inline">
                @csrf
                  <div class="form-group">
                    <label >Documento: </label><br><br><br>
                    <input type="number" name="document" class="form-control" placeholder="Buscar por Documento">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-outline-info"><i class="material-icons">search</i></button>
                    <a href="{{url(Auth::user()->urlUserAll())}}" class="btn btn-outline-warning"><i class="material-icons">list</i></a><br>
                    <a href="{{url(Auth::user()->urlUserCreate())}}" class="btn btn-outline-success" data-toggle="modal" data-target="#crear"><i class="bi bi-pencil-square"></i></a>
                  </div>
              </form>
     </div>
        <div class="card">
          <div class="card-header card-header-info">
            <h4 class="card-title ">Usuarios</h4>
            <p class="card-category"> Aqui esta toda la informacion de los usuarios del sistema</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-info">
                  <th>Nombre</th>
                  <th>Apellido</th>
                  <th>Documento</th>
                  <th>Telefono</th>
                  <th>Dirección</th>
                  <th>Email</th>
                  <th>Estado</th>
                  <th>Rol</th>
                  <th>Acciones</th>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                      <td>{{$user->name}}</td>
                      <td>{{$user->lastname}}</td>
                      <td>{{$user->document}}</td>
                      <td>{{$user->phone}}</td>
                      <td>{{$user->address}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->status->name}}</td>
                      <td>{{$user->role->name}}</td>
                      <td>
                        <a class="btn btn-info btn-sm my-2"  
                        href="{{url(Auth::user()->urlUserEdit($user->id))}}"><i class="material-icons">edit</i>
                        </a>
                      
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="crear" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-info">
          <h5 class="modal-title text-white" >Crear Empleado</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        @if($errors->any())
          <div class="alert alert-warning" role="alert">
            @foreach ($errors->all() as $error)
              <div>{{ $error }}</div>
            @endforeach
          </div>
        @endif </br> 
        <form action="{{url(Auth::user()->urlUserAll())}}" method="post" class="needs-validation" novalidate>
          @csrf
        <div class="container">
          <div class="row">
            <div class="col">
              <label for="validationCustom01">Nombre</label>
              <input type="text" name="name"  class="form-control" id="validationCustom01" required>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
            <div class="col">
              <label >Apellido</label>
              <input  type="text" name="lastname"  class="form-control" id="validationCustom01" required>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <label>Telefono</label>
              <input  type="number" name="phone" class="form-control" id="validationCustom01"  required>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
            <div class="col">
              <label >Documento</label>
              <input  type="number" name="document" class="form-control" id="validationCustom01" required>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <label>Dirección</label>
              <input  type="text" name="address"  class="form-control" id="validationCustom01" required>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete el recuadro
              </div>
            </div>
            <div class="col">
              <div class="row">
                <div class="col">
                  <label >Email</label>
                    <input  type="Email" name="email"  class="form-control" id="validationCustom01" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z0-9_]{1,5}">
                    <div class="valid-feedback">
                      Correcto!
                    </div>
                    <div class="invalid-feedback">
                      Complete el recuadro
                    </div>
                </div>
              </div>
            </div>
            <div class="col">
              <label>Rol</label>
              <select name="role_id"  class="form-control" id="validationCustom01" required>
                <option value="">Seleccione...</option>
                @foreach($roles as $role)
                  <option value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
              </select>
              <div class="valid-feedback">
                Correcto!
              </div>
              <div class="invalid-feedback">
                Complete todos los recuadros
              </div>
             </div>
            </div>
          </div>
          
          <div class="form-group">
            <button type="submit" class="btn btn-info float-right">Registrar</button> 
          </div>      
        </form>
                  </div>
                </div>
              </div>
          </div>
@endsection