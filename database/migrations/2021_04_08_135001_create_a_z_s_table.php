<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAZSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('a_z_s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number_AZ');
            $table->string('nombre_demanda');
            $table->string('apoderado');
            $table->mediumInteger('cantidad_items');
            $table->mediumInteger('cantidad_recobros');
            $table->bigInteger('vlr_base_capital');
            $table->bigInteger('gastos_administrativos_10');
            $table->bigInteger('cuantia');
            $table->string('RA');
            $table->string('RTA_RA');
            $table->string('conciliacion');
            $table->text('demanda_administrativa');
            $table->text('demanda_laboral');
            $table->text('pagos');
            $table->text('POS');
            $table->string('desistidos');
            $table->text('AGS');
            $table->text('investigacion');
            $table->text('adecuacion');
            $table->text('subsanacion');
            $table->string('prioridad_APF');
            $table->string('constancia');
            $table->string('demanda');
            $table->string('auto_admisorio_demanda');
            $table->string('notificacion_autoadmisorio_demanda_al_demandante');
            $table->string('notificacion_demanda_al_demandado');
            $table->string('numero_inicial_del_proceso_judicial');
            $table->string('nombre_inicial_del_despacho_judicial');
            $table->string('numero_actual_del_proceso_judicial');
            $table->string('nombre_actual_del_despacho_judicial');
            $table->text('pendiente_resolver_conflicto_de_competencia');
            $table->text('aprobado_por_glosa_pendiente_aprobacion_desistimiento');
            $table->text('en_investigacion_por_parte_de_la_contraloria');
            $table->string('numero_de_la_investigacion');
            $table->string('fecha_prestamo');
            $table->string('fecha_devolucion');
            $table->string('nombre_abogado');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('a_z_s');
    }
}
