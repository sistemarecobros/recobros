<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
           $this->call(Type_statusTableSeeder::class);
           $this->call(StatusTableSeeder::class);
           $this->call(RoleTableSeeder::class);
           
           DB::table('users')->insert([
            'name' => Str::random(10),
            'lastname' => Str::random(10),
            'document' => Str::random(10),
            'phone' => Str::random(10),
            'address' => Str::random(10),
            'status_id' => 1,
            'role_id' => 1,
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('recobros123'),
        ]);
    }
}
