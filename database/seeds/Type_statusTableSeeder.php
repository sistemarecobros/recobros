<?php

use Illuminate\Database\Seeder;
use App\Models\Type_status;

class Type_statusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type_statuses = array(
        	['name' => 'Usuario'],
        );

        foreach ($type_statuses as $value) {
        	$type_status = new Type_status;
        	$type_status->name = $value['name'];
        	$type_status->save();
        }
    }
}
