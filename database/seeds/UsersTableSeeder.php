<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
//Class user para crear nuevos usuarios
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 10; $i++) { 
            $user = new User;
            $user->name = $faker->name;
            $user->lastname = $faker->lastname;
            $user->document = $faker;
            $user->phone = $faker;
            $user->address = $faker->address;
            $user->email = $faker->email;
            $user->password = Hash::make('recobros123');
            $user->status_id = 1;
            $user->role_id = 1;
            $user->save();
        }

    }
}
