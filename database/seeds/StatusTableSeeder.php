<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = array(
        	['name' => 'Activo', 'description' => 'Puede utilizar el sistema','type_status_id' => 1],
        	['name' => 'Inactivo','description' => 'No puede utilizar el sistema','type_status_id' => 1],
        );

        foreach ($statuses as $value) {
        	$status = new Status;
        	$status->name = $value['name'];
            $status->description = $value['description'];
            $status->type_status_id = $value['type_status_id'];
        	$status->save();
        }
    }
}
