<?php

namespace App\Http\Middleware;

use Closure;

class HomeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $nameStatus = \Auth::user()->status->name;

        if($nameStatus === 'Activo')
            return $next($request);
        
        \Auth::logout();
        return redirect('login')->with('status', 'NO puedes usar el sistema');
    }
}
