<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AZ;
use App\User;

class AZController extends Controller
{
    public function index()
    {
        $azs = AZ::all();
        $users = User::all();
        return \View::make('azs\list',compact('azs','users'));
    }
    
    public function store(Request $request)
    {
        $az = new AZ;
        $az->number_AZ = $request->number_AZ;
        $az->nombre_demanda = $request->nombre_demanda;
        $az->apoderado = $request->apoderado;
        $az->cantidad_items = $request->cantidad_items;
        $az->cantidad_recobros = $request->cantidad_recobros;
        $az->vlr_base_capital = $request->vlr_base_capital;
        $az->gastos_administrativos_10 = $request->gastos_administrativos_10;
        $az->cuantia = $request->cuantia;
        $az->RA = $request->RA;
        $az->RTA_RA = $request->RTA_RA;
        $az->conciliacion = $request->conciliacion;
        $az->demanda_administrativa = $request->demanda_administrativa;
        $az->demanda_laboral = $request->demanda_laboral;
        $az->pagos = $request->pagos;
        $az->POS = $request->POS;
        $az->desistidos = $request->desistidos;
        $az->AGS = $request->AGS;
        $az->investigacion = $request->investigacion;
        $az->adecuacion = $request->adecuacion;
        $az->subsanacion = $request->subsanacion;
        $az->prioridad_APF = $request->prioridad_APF;
        $az->constancia = $request->constancia;
        $az->demanda = $request->demanda;
        $az->auto_admisorio_demanda = $request->auto_admisorio_demanda;
        $az->notificacion_autoadmisorio_demanda_al_demandante = $request->notificacion_autoadmisorio_demanda_al_demandante;
        $az->notificacion_demanda_al_demandado = $request->notificacion_demanda_al_demandado;
        $az->numero_inicial_del_proceso_judicial = $request->numero_inicial_del_proceso_judicial;
        $az->nombre_inicial_del_despacho_judicial = $request->nombre_inicial_del_despacho_judicial;
        $az->numero_actual_del_proceso_judicial = $request->numero_actual_del_proceso_judicial;
        $az->nombre_actual_del_despacho_judicial = $request->nombre_actual_del_despacho_judicial;
        $az->pendiente_resolver_conflicto_de_competencia = $request->pendiente_resolver_conflicto_de_competencia;
        $az->aprobado_por_glosa_pendiente_aprobacion_desistimiento = $request->aprobado_por_glosa_pendiente_aprobacion_desistimiento;
        $az->en_investigacion_por_parte_de_la_contraloria = $request->en_investigacion_por_parte_de_la_contraloria;
        $az->numero_de_la_investigacion = $request->numero_de_la_investigacion;
        $az->fecha_prestamo = $request->fecha_prestamo;
        $az->fecha_devolucion = $request->fecha_devolucion;
        $az->nombre_abogado = $request->nombre_abogado;
        $az->user_id = $request->user_id;
        $az->save();

        return redirect(\Auth::user()->urlAZAll());
    }

    public function show(Request $request)
    {
    	$azs = AZ::where('number_az','=', $request->number_az)->get();
        $users = User::all();
    	return \View::make('azs/list',compact('azs','users'));
    }

    public function create()
    {
        $users = User::all();
        return \View::make('azs/create',compact('users'));
    }

    public function edit($id)
    {
        $az = AZ::find($id);
        $users = User::all();
        return \View::make('azs/update',compact('az','users'));
    }

    public function destroy($id)
    {
        $az = AZ::find($id);
        $az->delete();
        return redirect()->back();
    }

   
    
    public function update($id, Request $request)
    {
        $az = AZ::find($id);
        $az->number_AZ = $request->number_AZ;
        $az->nombre_demanda = $request->nombre_demanda;
        $az->apoderado = $request->apoderado;
        $az->cantidad_items = $request->cantidad_items;
        $az->cantidad_recobros = $request->cantidad_recobros;
        $az->vlr_base_capital = $request->vlr_base_capital;
        $az->gastos_administrativos_10 = $request->gastos_administrativos_10;
        $az->cuantia = $request->cuantia;
        $az->RA = $request->RA;
        $az->RTA_RA = $request->RTA_RA;
        $az->conciliacion = $request->conciliacion;
        $az->demanda_administrativa = $request->demanda_administrativa;
        $az->demanda_laboral = $request->demanda_laboral;
        $az->pagos = $request->pagos;
        $az->POS = $request->POS;
        $az->desistidos = $request->desistidos;
        $az->AGS = $request->AGS;
        $az->investigacion = $request->investigacion;
        $az->adecuacion = $request->adecuacion;
        $az->subsanacion = $request->subsanacion;
        $az->prioridad_APF = $request->prioridad_APF;
        $az->constancia = $request->constancia;
        $az->demanda = $request->demanda;
        $az->auto_admisorio_demanda = $request->auto_admisorio_demanda;
        $az->notificacion_autoadmisorio_demanda_al_demandante = $request->notificacion_autoadmisorio_demanda_al_demandante;
        $az->notificacion_demanda_al_demandado = $request->notificacion_demanda_al_demandado;
        $az->numero_inicial_del_proceso_judicial = $request->numero_inicial_del_proceso_judicial;
        $az->nombre_inicial_del_despacho_judicial = $request->nombre_inicial_del_despacho_judicial;
        $az->numero_actual_del_proceso_judicial = $request->numero_actual_del_proceso_judicial;
        $az->nombre_actual_del_despacho_judicial = $request->nombre_actual_del_despacho_judicial;
        $az->pendiente_resolver_conflicto_de_competencia = $request->pendiente_resolver_conflicto_de_competencia;
        $az->aprobado_por_glosa_pendiente_aprobacion_desistimiento = $request->aprobado_por_glosa_pendiente_aprobacion_desistimiento;
        $az->en_investigacion_por_parte_de_la_contraloria = $request->en_investigacion_por_parte_de_la_contraloria;
        $az->numero_de_la_investigacion = $request->numero_de_la_investigacion;
        $az->fecha_prestamo = $request->fecha_prestamo;
        $az->fecha_devolucion = $request->fecha_devolucion;
        $az->nombre_abogado = $request->nombre_abogado;
        $az->user_id = $request->user_id;
        $az->save();
        return redirect(\Auth::user()->urlAZAll());
    }
}
