<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Status;
use App\Models\Role;
use Illuminate\Support\Facades\Mail;
use App\Mail\CrearUsuario;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        $roles = Role::where('id','<>',1)->get();
        return \View::make('users\list',compact('users','roles'));
    }

    
    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->document = $request->document;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->password = \Hash::make('recobros123');
        $user->status_id = 1;
        $user->role_id = $request->role_id;
        $user->save();

        Mail::to($request->email)->send(new CrearUsuario($user));
        return redirect(\Auth::user()->urlUserAll());
        
    
    }

    public function show(Request $request)
    {
    	$users = User::where('document','like','%'.$request->document.'%')->get();
        $roles = Role::all();
    	return \View::make('users/list',compact('users','roles'));
    }

    public function create()
    {
        $roles = Role::where('id',1)->get();
        return \View::make('users/list',compact('roles'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        $statuses = Status::where('type_status_id',1)->get();
        return \View::make('users/update',compact('user','roles','statuses'));
    }
    
    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->status_id = $request->status_id;
        $user->name    = $request->name;
        $user->lastname = $request->lastname;
        $user->phone = $request->phone;
        $user->document = $request->document;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->save();
        return redirect(\Auth::user()->urlUserAll());
    }
    
}
