<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','lastname','document','phone','address','role_id','status_id','email','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function azs()
    {
    	return $this->hasMany('App\Models\AZ');
    }

    //Users

    public function urlUserAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/user';
    }

    public function urlUserCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/user/create';
    }

    public function urlUserShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/user/show';
    }

    public function urlUserEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/user/'.$id.'/edit';
    }

    public function urlUserUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/user/update/'.$id;
    }

    //Az

    public function urlAZAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/az';
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/az';
        if(\Auth::user()->role->name === 'Abogado')
            return 'lawyer/az';
    }

    public function urlAZCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/az/create';
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/az/create';
    }

    public function urlAZShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/az/show';
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/az/show';
        if(\Auth::user()->role->name === 'Abogado')
            return 'lawyer/az/show';
    }

    public function urlAZEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/az/'.$id.'/edit';
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/az/'.$id.'/edit';
    }

    public function urlAZUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/az/update/'.$id;
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/az/update/'.$id;
    }

    public function urlAZDestroy($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/az/destroy/'.$id;
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/az/destroy/'.$id;
        
    }

    //Perfil

    public function urlUserEditPerfil($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/perfil/'.$id.'/edit';
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/perfil/'.$id.'/edit';
        if(\Auth::user()->role->name === 'Abogado')
            return 'lawyer/perfil/'.$id.'/edit';
    }

    public function urlUserUpdatePerfil($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'manager/perfil/update/'.$id;
        if(\Auth::user()->role->name === 'Empleado')
            return 'employee/perfil/update/'.$id;
        if(\Auth::user()->role->name === 'Abogado')
            return 'lawyer/perfil/update/'.$id;
        
    }

    public function urlUserHome()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'home';
        if(\Auth::user()->role->name === 'Empleado')
            return 'home';
        if(\Auth::user()->role->name === 'Abogado')
            return 'home';
    }
}
