<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AZ extends Model
{
    protected $table = 'a_z_s';
    protected $fillable = ['number_AZ',
                           'nombre_demanda',
                           'apoderado',
                           'cantidad_itmes',
                           'cantidad_recobros',
                           'vlr_base_capital',
                           'gastos_administrativos_10',
                           'cuntia',
                           'RA',
                           'RTA_RA',
                           'conciliacion',
                           'demanda_administrativa',
                           'demanda_laboral',
                           'pagos',
                           'POS',
                           'desistidos',
                           'AGS',
                           'investigacion',
                           'adecuacion',
                           'subsanacion',
                           'prioridad_APF',
                           'constancia',
                           'demanda',
                           'auto_admisorio_demanda',
                           'notificacion_autoadmisorio_demanda_al_demandante',
                           'notificacion_demanda_al_demandado',
                           'numero_inicial_del_proceso_judicial',
                           'nombre_inicial_del_despacho_judicial',
                           'numero_actual_del_proceso_judicial',
                           'nombre_actual_del_despacho_judicial',
                           'pendiente_resolver_conflicto_de_competencia',
                           'aprobado_por_glosa_pendiente_aprobacion_desistimiento',
                           'en_investigacion_por_parte_de_la_contraloria',
                           'numero_de_la_investigacion'];
    protected $guarded = ['id'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
