<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table    ='statuses';
    protected $fillable =['name','description'];
    protected $guarded  =['id'];

    public function users()
    {
    	return $this->hasMany('App\User');	
    }
}
